import string
from tkinter.messagebox import *
from tkinter import * 
chaineascii = string.ascii_lowercase + string.ascii_uppercase  #definition de la chaine caractère contenant toute les lettre de lalphabet

def cryptage(chaine, cle):                          ##fonction permettant de crypter une chaine de caractère en modifiant les caractère avec un décalage defini
    crypted_string = []                             ##definition d'une liste vide
    for caractere in chaine:                        ##découpage des caractère de la chaine de caratère
        valeur = ord(caractere)                     ##recuperation de la valeur ascii du caractère
        valeur = valeur + cle                       ##modification de valeur ascii
        crypted_string.append(chr(valeur))          ##ajoute le caractère modifie a la fin de la liste de caractère
    crypted_string = "".join(crypted_string)        ##Transforme la liste de caractère en chaine de caractère
    return crypted_string 
def decryptage(chaine,cle):                         ##fonction permettant de decrypter une chaine de caractère en modifiant les caractère avec un décalage defini
    decrypted_string = []                            ##definition d'une liste vide
    for caractere in chaine:                         ##découpage des caractère de la chaine de caratère
        valeur = ord(caractere)                      ##recuperation de la valeur ascii du caractère
        valeur = valeur - cle                        ##modification de valeur ascii
        decrypted_string.append(chr(valeur))         ##ajoute le caractère modifie a la fin de la liste de caractère
    decrypted_string = "".join(decrypted_string)     ##Transforme la liste de caractère en chaine de caractère
    return decrypted_string 
def cryptage_circulaire(chaine,cle):                ##fonction permettant de crypter une chaine de caractère en modifiant uniquement les lettres
    crypted_string = []                                         ##definition d'une liste vide
    chaineascii_crypter = chaineascii[cle:]+chaineascii[:cle]   ##Permet de crypter uniquement les lettre de l'alphabet et non les symbole, chiffre a l'aide de la cle passe en parametre
    for caractere in chaine:                                    ##découpage des caractère de la chaine de caratère
        try:
            index = chaineascii.index(caractere)                ##Recupere l'index du caractère dans la chaine non crypter(compose uniquement de lettre)
            crypted_string.append(chaineascii_crypter[index])   ##Ajoute a la liste de caractère la valeur, situe a l'index recupere precedement, dans la chaine crypter 
        except ValueError:                                      ## si la valeur n'est pas présente
             crypted_string.append(caractere)                   ##la copie sans transformation
    crypted_string = "".join(crypted_string)                    ##Transforme la liste de caractère en chaine de caractère
    return crypted_string
def decryptage_circulaire(chaine,cle):              ##fonction permettant de decrypter une chaine de caractère en modifiant uniquement les lettres
    decrypted_string = []                                       ##definition d'une liste vide
    chaineascii_crypter = chaineascii[cle:]+chaineascii[:cle]   ##Permet de crypter uniquement les lettre de l'alphabet et non les symbole, chiffre a l'aide de la cle passe en parametre
    chaineascii_decrypter = chaineascii_crypter[:cle]+chaineascii_crypter[cle:] ##Permet de decrypter la chaine crypter a l'aide de la cle passe en parametre
    for caractere in chaine:                                    ##découpage des caractère de la chaine de caratère
        try:
            index = chaineascii_decrypter.index(caractere)      ##Recupere l'index du caractère dans la chaine non crypter(compose uniquement de lettre)
            decrypted_string.append(chaineascii[index])         ##Ajoute a la liste de caractère la valeur, situe a l'index recupere precedement, dans la chaine crypter 
        except ValueError:                                      ## si la valeur n'est pas présente
             decrypted_string.append(caractere)
    decrypted_string = "".join(decrypted_string)                ##la copie sans transformation
    return decrypted_string                                     ##Transforme la liste de caractère en chaine de caractère

def callback_btn_cryptage():                        ##appuie sur le bouton cryptage non circulaire, lance l'appel a la fonction cryptage et affiche la chaine retourne
    value = entree.get("0.0", 'end').strip()
    cle = entree2.get("0.0", 'end').strip()
    cle = int(cle)
    message_final = cryptage(value, cle)
    textarea.delete(0.0,END)
    textarea.insert(END, message_final) 
def callback_btn_cryptage_circulaire():             ##appuie sur le bouton cryptage circulaire, lance l'appel a la fonction cryptage_circulaire et affiche la chaine retourne        
    value = entree.get("0.0", 'end').strip()
    cle = entree2.get("0.0", 'end').strip()
    try:
        cle = int(cle)
        message_final = cryptage_circulaire(value, cle)
        textarea.delete(0.0,END)
        textarea.insert(END, message_final) 
    except ValueError:
        showwarning('ValueError','Saisir la cle de cryptage')

def callback_btn_decryptage():                      ##appuie sur le bouton decryptage non circulaire, lance l'appel a la fonction decryptage et affiche la chaine retourne
    value = entree.get("0.0", 'end').strip()
    cle = entree2.get("0.0", 'end').strip()
    cle = int(cle)
    message_final = decryptage(value,cle)
    textarea.delete(0.0,END)
    textarea.insert(END, message_final) 
def callback_btn_decryptage_circulaire():           ##appuie sur le bouton cryptage circulaire, lance l'appel a la fonction cryptage_circulaire et affiche la chaine retourne          
    value = entree.get("0.0", 'end').strip()
    cle = entree2.get("0.0", 'end').strip()
    try:
        cle = int(cle)
        message_final = decryptage_circulaire(value, cle)
        textarea.delete(0.0,END)
        textarea.insert(END, message_final) 
    except ValueError:
        showwarning('ValueError','Saisir la cle de cryptage')

frame = Tk()
frame.title("Chiffre de Cesar")                     ##Création de l'interface

##Affichage des label contenant du texte
label = Label(frame, text="Veuillez saisir votre texte ci-dessous et choisir une méthode")
label.grid(column=1, row=0)
label2 = Label(frame, text="Veuillez saisir la cle de cryptage")
label2.grid(column=1,row=2)
label3 = Label(frame, text="Message traduit:")
label3.grid(column=1,row=4)

##zone de text dédie a l'écriture du message a traduire
entree = StringVar()
entree = Text(frame,width =30, height=10)
entree.grid(column=1, row=1)
##zone de text dédie a l'écriture de la cle
entree2 = StringVar()
entree2 = Text(frame,width =5, height=1)
entree2.grid(column=1, row=3)
## zone d'affichage du message traduit
textarea = Text(frame,width =30, height=10)
textarea.grid(row=5, column=1)

##definition des boutons, et des appel de fonction lors d'un appui sur ces dernier
button_cryptage = Button(frame, text="cryptage non circulaire",command=callback_btn_cryptage)
button_cryptage.grid(row=2, column=2)
button_decryptage = Button(frame, text="decryptage non circulaire",command=callback_btn_decryptage)
button_decryptage.grid(row=3, column=2)
button_cryptage = Button(frame, text="cryptage circulaire",command=callback_btn_cryptage_circulaire)
button_cryptage.grid(row=2, column=0)
button_decryptage = Button(frame, text="decryptage circulaire",command=callback_btn_decryptage_circulaire)
button_decryptage.grid(row=3, column=0)

frame.mainloop()