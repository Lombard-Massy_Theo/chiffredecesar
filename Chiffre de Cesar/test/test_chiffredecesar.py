import unittest
from chiffredecesar import cryptage
from chiffredecesar import decryptage
from chiffredecesar import cryptage_circulaire
from chiffredecesar import decryptage_circulaire

class TestFunctions(unittest.TestCase):
    
    def test_cryptage(self):
        self.assertEqual(cryptage("aaa",8), "iii")

    def test_decryptage(self):
        self.assertEqual(decryptage("iii",8), "aaa")

        def test_cryptage_circulaire(self):
            self.assertEqual(cryptage_circulaire("a1z",4), "e1D")

        def test_decryptage_circulaire(self):
            self.assertEqual(decryptage_circulaire("e1D",4), "a1z")